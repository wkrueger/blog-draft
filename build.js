const utl = require('buildscript-utils')
global.$promisify = utl.promisify

const tasks = {
    ts() {
        utl.spawn('./node_modules/.bin/tsc -p ./server-src')
        utl.spawn('./node_modules/.bin/tsc -p ./client-src')
    },

    tsw() {
        utl.spawn('./node_modules/.bin/tsc -w -p ./server-src')
        utl.spawn('./node_modules/.bin/tsc -w -p ./client-src')
    } ,
    runAllSql() {
        const database = require('./server-dist/common/database')
        const fs = require('fs')

        return database.init(true, true).then( () => {
            let connection = database.connection

            let files = fs.readdirSync('./sql')
                .filter( name => name.endsWith('.sql') && name !== 'zero.sql')
                .sort( (a,b) => {
                    let na = Number(a.replace(/[^0-9]/g), '')
                    let nb = Number(b.replace(/[^0-9]/g), '')
                    return na - nb
                }).map( name => {
                    return {
                        name ,
                        content : String(fs.readFileSync('./sql/' + name))
                    }
                })

            return files.reduce( (chain, file) => {
                return chain.then(() => {
                    console.log('running ', file.name)
                    return connection.execute(file.content)
                })
            }, Promise.resolve() )
        })
    }
}

utl.runTask(tasks)