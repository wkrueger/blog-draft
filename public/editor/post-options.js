var Editor;
(function (Editor) {
    var PostOptions;
    (function (PostOptions) {
        function show(opts, mainCb) {
            if (mainCb === void 0) { mainCb = function () { return null; }; }
            var $modal = $('<div class="ui small modal">'
                + '  <div class="header">Opções</div>'
                + '  <div class="content">'
                + '  <div class="ui error message"></div>'
                + '    <div class="ui form">'
                + '    </div>'
                + '  </div>'
                + '  <div class="actions">'
                + '    <div class="ui approve primary button">OK</div>'
                + '  </div>'
                + '</div>');
            var $error = $modal.find('.ui.error.message');
            $error.css('display', 'none');
            var callbacks = [];
            /**
             * Add "created date"
             */
            if (opts.created)
                createdFn(opts.created);
            function createdFn(created) {
                var date = new Date(created);
                var formatted = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()
                    + ' ' + fmtzeros(date.getHours()) + ':' + fmtzeros(date.getMinutes());
                var $append = $('<div class="field">'
                    + '  <label>Alterar data de criação (dd/mm/yyyy hh:mm)</label>'
                    + '  <input type="text"></input>'
                    + '</div>');
                var $text = $append.find('input[type=text]');
                $text.val(formatted);
                $modal.find('.ui.form').append($append);
                callbacks.push(function () {
                    var datesplit = $text.val().split(/[\s:\/]/g);
                    var newdate = new Date();
                    newdate.setDate(Number(datesplit[0]));
                    newdate.setMonth(Number(datesplit[1]) - 1);
                    newdate.setFullYear(Number(datesplit[2]));
                    newdate.setHours(Number(datesplit[3]));
                    newdate.setMinutes(Number(datesplit[4]));
                    if (isNaN(newdate.getTime()))
                        return;
                    opts.created = newdate;
                });
            }
            /**
             * Add "markdown break""
             */
            mdBreakFn(opts.markdown_break);
            function mdBreakFn(markdown_break) {
                var $append = $('<div class="field">'
                    + '  <label>Markdown: quebrar linha com apenas 1 enter</label>'
                    + '  <div class="ui checkbox">'
                    + '    <input type="checkbox" name="allow_html"></input>'
                    + '  </div>'
                    + '</div>');
                var $cb = $append.find('.ui.checkbox');
                $cb.checkbox();
                $cb.checkbox(markdown_break == true ? 'set checked' : 'set unchecked');
                $modal.find('.ui.form').append($append);
                callbacks.push(function () {
                    opts.markdown_break = $cb.checkbox('is checked') ? '1' : '';
                });
            }
            /**
             * Set up tags list.
             */
            tagsFn(opts.taglist || []);
            function tagsFn(taglist) {
                var $field = $('<div class="field">'
                    + '    <label>Marcadores</label>'
                    + '<div class="ui multiple fluid dropdown">'
                    + '    <input type="hidden" name="tags">'
                    + '    <i class="tag icon"></i>'
                    + '    <span class="text">Adicionar marcadores...</span>'
                    + '    <div class="menu">'
                    + '        <div class="ui icon input create-new-contain" style="width:initial">'
                    + '            <i class="plus icon"></i>'
                    + '            <input type="text" placeholder="Criar novo marcador..." class="create-new">'
                    + '        </div>'
                    + '        <div class="scrolling menu"></div>'
                    + '    </div>'
                    + '</div>'
                    + '</div>');
                var $select = $field.find('.ui.dropdown');
                var $container = $field.find('.scrolling.menu');
                var $containNew = $field.find('.create-new-contain');
                var $menu = $field.find('.menu');
                //menu never shows if there are no more options to add. hack it...
                $select.find('i.tag.icon').on('click', function () {
                    var toggle = $menu.css('display');
                    if (toggle == 'block') {
                        $menu.css('display', '');
                    }
                    else {
                        $menu.css('display', 'block').removeClass('hidden');
                    }
                });
                function createNew(value) {
                    Site.ajaxRequest({
                        method: 'POST',
                        url: '/api/admin/tag',
                        body: { name: value }
                    }).then(function (result) {
                        var newtag = {
                            id: result.id,
                            name: result.name,
                            selected: false
                        };
                        //this is mutable, propagate upwards
                        taglist.push(newtag);
                        pushTag(newtag);
                        $select.dropdown('refresh');
                        $menu.css('display', '');
                        $containNew.removeClass('loading');
                    }).catch(function (err) {
                        $containNew.removeClass('loading');
                        __handleError(Error('Erro. (essa tag já existe?)'));
                        throw err;
                    });
                }
                $field.find('.plus.icon').on('click', function () {
                    var val = $field.find('input.create-new').val();
                    if (val.length)
                        createNew(val);
                });
                $field.find('input.create-new').on('keydown', function (ev) {
                    var $this = $(this);
                    if (ev.which === 13 && this.value.length) {
                        $this.parent().addClass('loading');
                        ev.preventDefault();
                        ev.stopPropagation();
                        createNew(this.value);
                    }
                });
                var selected = [];
                function pushTag(tag) {
                    var $selectItem = $('<div class="item" data-value="' + tag.id + '">'
                        + '    <div class="ui gray empty circular label"></div>'
                        + tag.name
                        + '</div>');
                    $container.append($selectItem);
                    if (tag.selected)
                        selected.push(String(tag.id));
                }
                taglist.forEach(pushTag);
                $select.dropdown();
                setTimeout(function () {
                    $select.dropdown('set exactly', selected);
                }, 0);
                $modal.find('.ui.form').append($field);
                callbacks.push(function () {
                    var selected = $select.dropdown('get value');
                    if (!selected)
                        selected = [];
                    else
                        selected = selected.split(',');
                    return taglist.map(function (tag) {
                        tag.selected = selected.indexOf(String(tag.id)) !== -1;
                        return tag;
                    });
                });
            }
            /**
             * custom fields
             */
            function customFields() {
                opts.customFields = opts.customFields || {};
                function text(field) {
                    var $field = $("\n                    <div class=\"field\">\n                        <label></label>\n                        <input type=\"text\"></input>\n                    </div>\n                ");
                    $field.find('label').html(field.label);
                    $field.find('input').val(field.value);
                    callbacks.push(function () {
                        field.value = $field.find('input').val();
                    });
                    return $field;
                }
                function textarea(field) {
                    var $field = $("<div class=\"field\">\n                        <label></label>\n                        <textarea rows=\"3\"></textarea>\n                    </div>");
                    $field.find('label').html(field.label);
                    $field.find('textarea').val(field.value);
                    callbacks.push(function () {
                        field.value = $field.find('textarea').val();
                    });
                    return $field;
                }
                function image(field) {
                    var $field = $("\n                    <div class=\"field\">\n                        <label></label>\n                        <img class=\"ui medium bordered image\"\n                            style=\"margin-bottom:10px;margin-top: 10px; padding:10px;margin-left:auto; margin-right: auto;\">\n                        </img>\n                        <div class=\"ui icon input\">\n                            <i class=\"icon file image outline\"></i>\n                            <input type=\"text\"></input>\n                        </div>\n                    </div>\n                ");
                    var $input = $field.find('input');
                    var $img = $field.find('img');
                    $field.find('label').html(field.label);
                    $img.attr('src', field.value).css('display', field.value ? 'block' : 'none');
                    $input.val(field.value);
                    $input.on('click', function () {
                        Editor.AddPicture.showDialog(function (link) {
                            $input.val(link);
                            $field.find('img')
                                .attr('src', link)
                                .css('display', link ? 'block' : 'none');
                            setTimeout(function () {
                                $modal.modal('refresh');
                            }, 500);
                        })();
                    });
                    callbacks.push(function () {
                        field.value = $input.val();
                    });
                    return $field;
                }
                var builders = { text: text, image: image, textarea: textarea };
                if (Object.keys(opts.customFields).length) {
                    $modal.find('.ui.form')
                        .append("<h3 class=\"ui dividing header\">Campos de temas</h2>");
                }
                for (var it in opts.customFields) {
                    var field = opts.customFields[it];
                    var fn = builders[field.type];
                    if (!fn)
                        return;
                    var $field = fn(field);
                    $modal.find('.ui.form').append($field);
                }
            }
            customFields();
            $modal.find('.ui.approve').on('click', function () {
                callbacks.forEach(function (cb) { return cb(); });
                mainCb();
            });
            $modal.modal({
                allowMultiple: true,
                onVisible: function () {
                    $modal.modal('refresh');
                }
            });
            $modal.modal('show');
            // private -------
            function __handleError(err) {
                $error.empty();
                $error.append('<p>' + err.message + '</p>');
                $error.css('display', 'block');
                throw err;
            }
        }
        PostOptions.show = show;
    })(PostOptions = Editor.PostOptions || (Editor.PostOptions = {}));
})(Editor || (Editor = {}));
function fmtzeros(num, nzeros) {
    if (nzeros === void 0) { nzeros = 2; }
    num = String(num);
    while (num.length < nzeros) {
        num = '0' + num;
    }
    return num;
}
