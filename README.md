# What's this?

This was a mere code sample. I then became a blog. It will turn into a CMS.

# Startup

**Prerequisites**  
- node v6+
- mysql

**Features**

  - Write articles using markdown  
  - Save drafts for later publishing  
  - Mobile-friendly editor  
  - Image upload  
  - Hooks for creating themes (alpha). See the wiki.  
    - Set up custom routes, wire them up to theme models  
    - Set up custom fields in themes
    - Set up custom lists using tags

**Quickstart**

On windows you need a working node-gyp for building bcrypt.
Achieve that by installing [the miraculous windows build tools](https://github.com/felixrieseberg/windows-build-tools).

- `npm i`  
- Run the database installs: `node build runAllSql`
- Set the required environment variables (on your editor debugger, or create a sh script, or use pm2, etc)  
  - you may just put placeholders on the email server ones.
- Compile typescript (`node build ts`)

Should be it. Run `node .`

# Configurable environment variables

> asterisk means required

## App

    APP_BASE_URL *
    APP_HTTP_POST -- set up express port, defaults to 8020
    APP_DB_HOST
    APP_DB_USER
    APP_DB_DB
    APP_DB_PASS
    APP_LIVERELOAD -- adds the livereload header for development
    APP_DEMO_MODE  -- specify user id to be always logged in    

## Email

Email is only used atm to create the app users.

    APP_EMAIL_FROMADDR *
    APP_EMAIL_METHOD *

> Either one of the following options must be configured (or set up with a placeholder)

### SMTP Email

You may point this to an email account you own. Gmail wont work out for the box for it has
more security measures, but Outlook will.

    APP_EMAIL_USER  
    APP_EMAIL_PASS  

### Mailgun

You may create a free mailgun account. You can use mailgun without buying a domain
by using their "sandbox domain" feature.

    APP_EMAIL_MAILGUN_KEY
    APP_EMAIL_MAILGUN_DOMAIN  
