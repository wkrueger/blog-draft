USE `blog-draft`;

DROP TABLE routes;

CREATE TABLE `routes` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(20) NOT NULL,
	`seq` SMALLINT(6) NOT NULL DEFAULT '100',
	`route` VARCHAR(50) NOT NULL,
	`theme` VARCHAR(30) NOT NULL,
	`model` VARCHAR(30) NOT NULL,
	`param` VARCHAR(50) NULL DEFAULT NULL,
	`main` TINYINT(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `route` (`route`),
	UNIQUE INDEX `name` (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;