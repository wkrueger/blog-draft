USE `blog-draft`;
CREATE TABLE `routes` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`seq` SMALLINT(6) NOT NULL DEFAULT '100',
	`route` VARCHAR(50) NOT NULL,
	`theme` VARCHAR(30) NOT NULL,
	`model` VARCHAR(30) NOT NULL,
	`param` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `route` (`route`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
