USE `blog-draft`;
CREATE TABLE `tags` (
	`id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;



CREATE TABLE `articles_tags` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`article` INT(11) NOT NULL,
	`tag` SMALLINT(6) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `tag_vs_article` (`article`, `tag`),
	INDEX `articles_tags:tag` (`tag`),
	CONSTRAINT `articles_tags:article` FOREIGN KEY (`article`) REFERENCES `articles` (`id`) ON DELETE CASCADE,
	CONSTRAINT `articles_tags:tag` FOREIGN KEY (`tag`) REFERENCES `tags` (`id`) ON DELETE CASCADE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;