USE `blog-draft`;

ALTER TABLE `articles`
	ADD COLUMN `custom_fields` JSON NULL AFTER `markdown_break`,
	DROP COLUMN `github_user`,
	DROP COLUMN `github_repo`,
	DROP COLUMN `github_path`;
