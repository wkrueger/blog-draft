interface JQuery {
    dropdown(...i): any
    form(...i): any
    modal(...i): any
    dimmer(...i): any
    checkbox(...i): any
}

interface Location {
    go(...i): void
}