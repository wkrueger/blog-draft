namespace Editor {

    export function defaultMDEConfig(element) {
        return {
            element: element,
            spellChecker: false,
            renderingConfig: { codeSyntaxHighlighting: true },
            toolbar: undefined
        }
    }



    export function editorHack(editor) {

        var $toolbar = $('.editor-toolbar')


        /**
         * Editor auto-scaling
         */
        setTimeout(function(){
            fitHeight(document.querySelector('.CodeMirror'), 100)
        },0)
        window.addEventListener('resize', function() {
            fitHeight(document.querySelector('.CodeMirror'), 100)
        })


        /**
         * Override add picture button (image selection dialog)
         **/
        var addPictureEl =  $toolbar.find('.fa.fa-picture-o')[0]
        addPictureEl.onclick = AddPicture.showDialog(absPath => {
            var codeMirrorDoc = editor.codemirror.doc
            codeMirrorDoc.replaceSelection('![descrição da imagem](' + absPath + ')\n')
        })


        /**
         * Monkey-patck the toolbar
         **/
        if (window.innerWidth < 700) {
                var $left = $toolbar.find('.fa.fa-quote-left')
                $left.next().remove()
                $left.next().remove()
                $left.remove()
                $toolbar.find('i.separator').remove()

                var $disable = $toolbar.find('.fa.fa-eye.no-disable')
                $disable.next().remove()
                var $newEl = $(
                    '<a title="Fullscreen" tabindex="-1" class="fa fa-arrows-alt no-disable"></a>'
                ).click(function(event) {
                    editor.toggleFullScreen()
                })
                $toolbar.find('.fa.fa-bold')
                    .before($disable)
                    .before($newEl)
                    .before('<i class="separator">|</i>')
                editor.toolbarElements.fullscreen = $newEl[0]
        }


        function fitHeight(element, offset) {
            offset = offset || 20
            var currentY = $(element).offset().top - window.pageYOffset
            element.style.height = (window.innerHeight - currentY - offset) + 'px'
        }
    }


    /**
     * Sets up the submitter button
     */
    export function initSubmitter($element:JQuery, isDraft:boolean, form) {
        var $submit = $element.find('.active-submit')
        var $dropdown = $element.find('.ui.floating.dropdown')
        var selected

        if (isDraft) toDraft()
        else toPublish()


        $element.find('.item[data-id=publish]').on('click', toPublish)
        $element.find('.item[data-id=draft]').on('click', toDraft)


        function toDraft() {
            selected = 'draft'
            form.elements.status.value = selected
            $submit.add($dropdown)
                .removeClass('primary')
                .addClass('teal')
            $submit.html('Salvar rascunho')
        }

        function toPublish() {
            selected = 'published'
            form.elements.status.value = selected
            $submit.add($dropdown)
                .removeClass('teal')
                .addClass('primary')
            $submit.html('Publicar')
        }
    }


    /**
     * Set up draft auto saving
     */
    export function localDraft(editor, edited, id, noLocalDraft) {
        id = id || 'NEW'

        if (id === 'NEW') (Site.submitHooks||[]).push(function(){
            localStorage.removeItem('article:NEW')
        })

        $('#revertBtn').hide()

        setInterval( function() {
            var tosave = {
                edited : new Date() ,
                content : editor.value()
            }
            localStorage.setItem('article:' + id, JSON.stringify(tosave))
            $('#revertBtn').show()
            console.log('saved local draft')
        }, 1000 * 30)

        if (noLocalDraft) return

        loadDraft()

        let showNag = localStorage.getItem('once:localDraft')
        if (!showNag) {
            nagModal()
        }

        function nagModal() {
            var $modal = $(
                '<div class="ui small modal">'
                + '  <div class="header">Sobre o rascunho local</div>'
                + '  <div class="content">'
                + '    <p>O sistema automaticamente salva um rascunho do artigo'
                + '       de tempos em tempos, mesmo que você não tenha clicado em "Salvar" ou "Publicar".</p>'
                + '    <p>Caso queira ver a última versão que você mandou salvar (ao invés do último rascunho salvo), use o botão "Reverter".</p>'
                + '  </div>'
                + '  <div class="actions">'
                + '    <div class="ui approve primary button">OK</div>'
                + '  </div>'
                + '</div>')
            localStorage.setItem('once:localDraft', '1')
            $modal.modal('show')
        }


        $('#revertBtn').on('click', function(event) {
            var $modal = $(
                '<div class="ui small modal">'
                + '  <div class="header">Reverter</div>'
                + '  <div class="content">'
                + '    <p>Isto irá reverter seu texto para o último estado salvo no servidor.</p>'
                + '  </div>'
                + '  <div class="actions">'
                + '    <div class="ui approve primary button">OK</div>'
                + '    <div class="ui cancel button">Cancelar</div>'
                + '  </div>'
                + '</div>')

            $modal.find('.approve.button').on('click', function () {
                let current = window.location.href.split('?')[0]
                window.location.href = current + '?fresh=1'
            })

            $modal.modal('show')
        })


        function loadDraft() {
            var stored
            try {
                var raw = localStorage.getItem('article:' + id)
                stored = JSON.parse(raw)
            } catch(err) {
                return
            }
            if (!stored) return
            var savedTime = new Date(edited)
            var localstoredTime = new Date(stored.edited)
            //localstoredTime.setMinutes(localstoredTime.getMinutes() + _offset)
            if (localstoredTime.getTime() > (savedTime.getTime() || 0)) {
                editor.value(stored.content)
                if (id !== 'NEW') $('#revertBtn').show()
            }
        }
    }

}