namespace Editor.AddPicture {

    const __editorModalHtml = $.get('/assets/editor/editor-modal.html')

    type readyCbT = (imagePath: string) => any

    export const showDialog = (readyCb:readyCbT) => () => {
        __editorModalHtml.then(function (resp) {

            var $modal = $(resp)
            var $button = $modal.find('#uploadButton')
            var $input = $button.find('input[type=file]')
            var $delete = $modal.find('#imgDeleteButton')
            var $content = $modal.find('#content')
            var _iconsActive = false

            $button.on('click', function () {
                $input[0].click()
            })

            $input.on('change', function () {
                var files = this.files
                if (!(files && files.length)) return
                var reader = new FileReader()
                reader.onload = function (readerEv) {
                    $content.dimmer('show')
                    Site.ajaxSendRaw(
                        'PUT',
                        '/api/file/' + encodeURIComponent(files[0].name),
                        readerEv.target['result']
                    ).then(function () {
                        $content.dimmer('hide')
                        //TODO show message
                        return _populateList()
                    })
                    .catch(_handleError)
                }
                reader.readAsArrayBuffer(files[0])
            })


            $delete.on('click', function (event) {
                if (_iconsActive) return
                _iconsActive = true
                $modal.find('#content .inner .media-item').append(
                    '<div class="ui top right attached red label remove" style="width:35px"><i class="icon remove"></i></div>'
                ).on('click', function(event) {
                    $content.dimmer('show')
                    Site.ajaxRequest({
                        method: 'DELETE',
                        url: '/api/file/' + encodeURIComponent($(this).attr('data-file'))
                    }).then(function () {
                        _populateList()
                        $content.dimmer('hide')
                    })
                    .catch(_handleError)
                })
            })


            $modal.modal({ allowMultiple: true })
            $modal.modal('show')
            _populateList().catch(_handleError)


            // "private" ------------------------------------

            function _populateList() {
                return Site.ajaxRequest({
                    method: 'GET',
                    url : '/api/file'
                })
                .then(function (resp:any) {
                    if (!resp.files) throw Error('Erro ao mostrar arquivos.')
                    if (!resp.files.length) {
                        $modal.find('#content .inner').empty().append(
                            '<h1 style="margin:20px;color:#c5c5c5;text-align:center;">' +
                            'Nenhum conteúdo.' +
                            '</h1 >'
                        )
                        return
                    }
                    var html = $('<div class="ui four column grid stackable"></div>')
                    resp.files.forEach(function (file) {
                        var style = 'background-image: url(/media/thumbnails/'+ encodeURIComponent(file) +');'
                            + 'background-repeat: no-repeat;'
                            + 'background-size: contain;'
                            + 'background-position: top;'
                            + 'height:130px;'
                            + 'margin:15px;'
                            + 'cursor:pointer;'

                        var $el = $('<div class="column media-item" style="'+ style +'">' +
                            '<div class="ui bottom left attached label filename-label" style="bottom:-15px">' + file + '</div>' +
                            '</div>')
                        $el.attr('data-file', file)
                        $el.on('click', function (event) {
                            if ($(event.target).hasClass('remove')) {
                                return false
                            }
                            //var fn = previousAddPictureEv.bind(this)
                            //fn(event)
                            var absPath = '/media/' + file
                            readyCb(absPath)
                            $modal.modal('hide')
                        })
                        html.append($el)
                        _iconsActive = false
                    })
                    $modal.find('#content .inner').empty().append(html)
                })
            }


            function _handleError(err) {
                $content.dimmer('hide')
                var $error = $modal.find('.ui.error')
                $error.removeClass('hidden')
                $error.find('.content').html(err.error.message)
                throw err
            }

        })
    }

}