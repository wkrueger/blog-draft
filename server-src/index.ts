// ---- module shim for custom paths

var Module = require('module');
var originalRequire = Module.prototype.require;

Module.prototype.require = function(this:any, ...args){
    let path = args[0]
    if (path.charAt(0) === '@') path = __dirname + '/' + path.substr(1)
    args[0] = path
    return originalRequire.apply(this, args);
};


// ---- end module shim

import express = require('express')
const app = express()
import ono = require('ono')
import helmet = require('helmet')
app.use(helmet())

import database = require('./common/database')
import roles = require('./common/roles')
import config = require('./common/config')
import slink = require('./common/slink')
import themes = require('./common/themes')
import '@common/mail' //init

import favicon = require('serve-favicon')
import compression = require('compression')

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

global['$debugMw'] = (msg) => (req, res, next) => {
    console.log('DEBUGMW layer route', msg||'')
    next()
}


//replace with a logger in the future
global['$log'] = console.log


/**
 * Shorthand for checking parameters, adds underline for convention
 */
global['$checkParams'] = function(obj, ...params) {
    let out = {}
    out = params.reduce( (previous, param) => {
        if (obj[param] === undefined) {
            throw ono({code:'MISSING_PARAM', param}, `Parâmetro -${param}- está faltando.`)
        }
        previous[param] = obj[param]
        return previous
    }, {})

    return out
}


//tomelhe global. TODO arrumar essa zona
global['$promisify'] = function( fn, ...args ) {
    return new Promise( (resolve, reject) => {
        fn( ...args , (err, ...result) => {
            if (err) return reject(err)
            resolve(result)
        })
    })
}


async function measure(description, fn) {
    //var start = process.hrtime()
    await fn()
    //let elapsed = process.hrtime(start)
    //let elapseds = (elapsed[0] + elapsed[1] / 1e9).toFixed(3) + 's';
    //$log(description/*, 'in', elapseds*/)
}


measure('Init database', database.init).then(() => {
    return Promise.all([
        measure('Init roles', roles.init),
        measure('Init configs', config.init),
        measure('Init slink', slink.init),
        measure('Init themes', themes.init)
    ])
}).then(() => {

    const pages = require('./components/pages')

    pages.pagesInit(app)

    app.use(favicon(`${__dirname}/../public/favicon/favicon.ico`))

    app.use(compression())
    const staticConf = process.env.NODE_ENV === 'production' ? { maxAge : '1d' } : {}
    app.use('/assets', express.static('../public', staticConf))
    app.use('/media', express.static('../user-content/media', staticConf))
    app.use('/template-media', express.static('../user-content/template-media', staticConf))

    const session = require('./common/auth-mw')

    app.use(session.initSession)

    app.use('/pages', pages.app)
    app.use('/api', require('./components/api'))
    app.get('/login', (req, res) => res.redirect('/pages/admin'))
    app.get('/admin', (req, res) => res.redirect('/pages/admin'))

    //custom routes
    app.get('*',
        themes.customRoutesHandler,
        pages.errorHandler
    )


    app.get('/', (req, res) => {
        return res.redirect('/pages/article/list')
    })

    //404 handler
    app.use( (req, res) => {
        res.status(404).send(`NADA ACHADO: ${req.method} ${req.url}`)
    })

    //iniciar o servidor só depois de ter conectado com o mysql
    let HTTP_PORT = process.env.APP_HTTP_PORT || 8020
    app.listen( HTTP_PORT , () => console.log('HTTP Server up at ' + HTTP_PORT) )
}).catch( err => {
    console.error('Error when starting the server.')
    console.error(err)
    throw err
})


process.on('unhandledRejection', function(reason, p){
    console.log("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
    // application specific logging here
    process.exit()
});
