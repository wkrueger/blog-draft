/**
 * Admin article pages (only edit and create)
 */
import auth = require('@common/auth-mw')
import express = require('express')
import marked = require('marked')
import highlight = require('highlight.js')
import db = require('@common/database')
import { tagList } from '@common/tags'
import themes = require('@common/themes')


marked.setOptions({
    highlight: function (code) {
        return highlight.highlightAuto(code).value;
    }
})
export var router = express.Router()

router.get('/new', auth.authGate, async (req, res, next) => {
    try {
        var taglist = await tagList(0)
        res.render('v-new', {
            title: 'Novo post',
            $scripts: ['/assets/editor/editor.js', '/assets/editor/add-picture.js', '/assets/editor/post-options.js'],
            taglist: escape(JSON.stringify(taglist)),
            customFields : escape(JSON.stringify(themes.customFieldsIdx))
        })
    } catch (err) {
        next(err)
    }
})


router.get('/:pageId/edit', auth.authGate, async (req, res, next) => {
    try {
        if (!req.params.pageId) throw Error('Página não informada.')
        let [rows] = await db.connection.execute(
            'SELECT * FROM articles WHERE id = ? LIMIT 1',
            [req.params.pageId])
        if (!rows.length) throw Error('Artigo não existe.')
        var taglist = await tagList(req.params.pageId)
        let saved : any = rows[0].custom_fields || {}
        let customfields = themes.customFields.reduce((out, field) => {
            let fieldDef = Object.assign({}, field)
            fieldDef.value = saved[fieldDef.key]
            out[fieldDef.key] = fieldDef
            return out
        }, {})
        let article = rows[0]
        res.render('v-edit', {
            title: 'Editando artigo :: ' + article.title ,
            article,
            $scripts: ['/assets/editor/editor.js', '/assets/editor/add-picture.js', '/assets/editor/post-options.js'],
            noLocalDraft: req.query.fresh ? '1' : '',
            taglist: escape(JSON.stringify(taglist)),
            customFields : escape(JSON.stringify(customfields))
        })
    } catch (err) {
        next(err)
    }
})