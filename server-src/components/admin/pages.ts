import express = require('express')
import { authGate } from '@common/auth-mw'
import config = require('@common/config')
import { connection as db } from '@common/database'
import moment = require('moment')
import { AllHtmlEntities } from 'html-entities'
import themes = require('@common/themes')
import qs = require('qs')
import { tagList } from '@common/tags'
const entities = new AllHtmlEntities()

const admin = express.Router()
export = admin;


admin.use(authGate)


var N_ARTICLES = 5

admin.get('/articles', async (req, res, next) => {
    try {
        //mvc é um padrão opressor imposto pela burguesia
        let [count_res] = await db.execute(`SELECT COUNT(id) AS cnt FROM articles`)
        let count = Number(count_res[0].cnt)|| 0
        let [articles] = await db.execute(`SELECT
            id, title, trimmed_content, created, status
            FROM articles
            ORDER BY created DESC
            LIMIT ?, ${N_ARTICLES}`, [Number(req.query.skip) || 0])
        articles = await Promise.all( articles.map( async (article) => {
            article.formattedDate = moment(article.created).format(config.config.timestamp_format)
            article.formattedContent = entities.encode(article.trimmed_content)
            article.isDraft = article.status == 'draft'
            var taglist = await tagList(article.id)
            article.tags = taglist.filter( tag => tag.selected ).map( tag => tag.name )
            return article
        }))
        var torender : any = {
            layout: 'flat-layout',
            articles
        }
        let skip = Number(req.query.skip) || 0
        if (skip > 0) torender.prev = String(Math.max(skip - N_ARTICLES , 0))
        let next = skip + articles.length
        if (next < count) torender.next = next
        res.render('v-articles', torender)
    } catch (err) {
        next(err)
    }
 })


admin.get('/config', (req, res) => {
    var _config = <any>Object.assign({}, config.config)
    _config.allow_html = _config.sanitize_markdown == true ? false : true
    _config.display_name = req.session!.userName
    res.render('v-config', {
        layout: 'flat-layout' ,
        config : JSON.stringify(_config)
    })
})


admin.get('/theme-list', (req, res) => {
    res.render('v-theme-list', {
        themes: themes.themes,
        layout: 'flat-layout',
        themejson: JSON.stringify(themes.themes),
        routes: themes.routes.map(route => {
            let currentTheme = themes.themesIdx[route.theme]
            if (!currentTheme) return
            route['_theme'] = currentTheme
            route['_model'] = currentTheme.models.filter(model => {
                return model.key == route.model
            })[0]
            route['_qs'] = qs.stringify(route.param)
            return route
        })
    })
})


admin.get('/theme-detail', (req, res) => {
    var selected = themes.themes.filter(theme => theme._reference == req.query.ref)[0]
    selected.models = selected.models.map(model => {
        model.type = <any>themes.ModelType[model.type]
        return model
    })
    res.render('v-theme-detail', {
        theme: selected,
        layout: 'flat-layout'
    })
})


admin.get('/change-password', (req, res) => {
    res.render('v-change-password', {
        layout: 'flat-layout'
    })
})


admin.get('/tag-list', async (req, res, next) => {
    try {
        var [tags] = await db.execute('SELECT id, name FROM tags')
        res.render('v-tag-list', {
            tags, layout: 'flat-layout'
        })
    } catch (err) {
        next(err)
    }
})


admin.get('/', (req, res) => {
    res.render('v-main', { title : 'Admin' })
})