import express = require('express')
import config = require('@common/config')
import themes = require('@common/themes')
import { connection as db } from '@common/database'
import tags = require('@common/tags')

const api = express.Router()
export = api


api.put('/config', async (req, res, next) => {
    try {
        const fields = ['main_title', 'main_subtitle', 'timestamp_format', 'sanitize_markdown']
        $checkParams(req.body, ...fields)
        var all: Promise<any>[] = fields.map(field => {
            return config.set(<any>field, req.body[field])
        })
        await Promise.all(all)
        if (req.body.display_name) {
            await db.execute('UPDATE users SET display_name = ? WHERE id = ?',
                [req.body.display_name, req.session!.userId || ''])
            req.session!.userName = req.body.display_name
        }
        res.status(200).send({ status : 'OK' })
    } catch (err) {
        next(err)
    }
})



api.get('/themes', async (req, res, next) => {
    try {
        res.status(200).send({ themes: themes.themes })
    } catch (err) {
        next(err)
    }
})


api.post('/route', async (req, res, next) => {
    try {
        var { name, route, theme, model } = $checkParams(req.body,
            'route', 'theme', 'model', 'name')
        await themes.setRoute({
            name, route, theme, model,
            param: req.body.param,
            main: Number(req.body.main) || 0
        })
        res.status(200).send({ status : 'OK' })
    } catch (err) {
        next(err)
    }
})


api.delete('/route/:id', async (req, res, next) => {
    try {
        var { id } = $checkParams(req.params, 'id')
        await themes.deleteRoute(id)
        res.status(200).send({ status : 'OK' })
    } catch (err) {
        next(err)
    }
})


api.get('/route-up/:id', async (req, res, next) => {
    try {
        var { id } = $checkParams(req.params, 'id')
        id = Number(id)
        await themes.upRoute(id)
        res.status(200).send({ status : 'OK' })
    } catch (err) {
        next(err)
    }
})


api.get('/route-down/:id', async (req, res, next) => {
    try {
        var { id } = $checkParams(req.params, 'id')
        id = Number(id)
        await themes.downRoute(id)
        res.status(200).send({ status : 'OK' })
    } catch (err) {
        next(err)
    }
})


api.post('/tag', async (req, res, next) => {
    try {
        $checkParams(req.body, 'name')
        var {id} = await tags.createTag(req.body.name)
        res.status(200).send({ id , name: req.body.name })
    } catch (err) {
        next(err)
    }
})


api.delete('/tag/:tagId', async (req, res, next) => {
    try {
        var { tagId } = $checkParams(req.params, 'tagId')
        await db.execute('DELETE FROM tags WHERE id = ?', [Number(tagId)])
        res.status(200).send({ status: 'OK' })
    } catch (err) {
        next(err)
    }
})