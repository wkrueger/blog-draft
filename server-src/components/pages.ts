import express = require('express')
export const app = express.Router()
import { config } from '@common/config'
import path = require('path')
import glob = require('glob')
import { linkHelper, themeresourceHelper, inflateHelper } from '@common/themes/pages'

//handlebars setup
import ehbs = require('express-handlebars')

export function pagesInit(app) {

    let hbsInstance:any = ehbs.create({
        extname: '.hbs' ,
        defaultLayout : 'main-layout' ,
        layoutsDir: '../server-src/components/_views/layouts',
        helpers : {
            link: linkHelper ,
            yesno(param) {
                return param ? 'Sim' : ''
            },
            editlink(id) {
                return '/pages/article/' + id + '/edit'
            },
            themeresource: themeresourceHelper,
            inflate: inflateHelper,
            json(query, ...params) {
                let meta = params.slice(-1)[0] || {}
                let dataz = meta.data.root[query]
                return escape(JSON.stringify(dataz))
            }
        },
    })

    hbsInstance.partialsDir = [
        '../server-src/components/_views/partials' ,
        ...glob.sync('../user-content/template-media/**/views')
    ]

    app.engine('.hbs', hbsInstance.engine);
    app.set('view engine', '.hbs')

    //just in this case, relative to project root, not to cwd
    var ROOT_PATH = path.resolve( process.cwd(), '..' )
    var TEMPLATE_PATH = path.resolve( ROOT_PATH, 'user-content', 'template-media' )
    var COMPONENTS_PATH = path.resolve( ROOT_PATH, 'server-src', 'components' )

    app.set('views', ROOT_PATH)

    /**
     * Override res.render. Reroute views. Add global variables.
     */
    const oldRender = app.response.render
    app.response = <any>{
        __proto__ : app.response ,
        render(this: express.Response, modelpath:string, data = {}, ...params) {
            var session: Express.Session = <any>this.req.session || {}
            let extendedParams : Themes.TemplateRoot = <any>Object.assign(data, {
                $online : session.userId !== undefined ,
                $userName: session.userName,
                $liveReload: process.env.APP_LIVERELOAD > 0,
                $blogTitle: config.main_title,
                $blogSubtitle: config.main_subtitle,
                $currentUrl: process.env.APP_BASE_URL + this.req.originalUrl,
                $baseUrl: process.env.APP_BASE_URL,
                $customRoute: this.req.customRoute || {}
            })

            let url = this.req.originalUrl!.split('/').slice(1)
            if (modelpath.charAt(0) === '@') {
                let newpath = path.resolve( COMPONENTS_PATH, '_views', modelpath.substr(1))
                newpath = path.relative( ROOT_PATH, newpath )
                return oldRender.bind(this)(newpath , extendedParams, ...params)
            }

            if (modelpath.charAt(0) === '#') {
                let newpath = path.resolve( TEMPLATE_PATH , modelpath.substr(1) )
                newpath = path.relative( ROOT_PATH, newpath )
                return oldRender.bind(this)( newpath , extendedParams, ...params )
            }

            let newpath = path.resolve(COMPONENTS_PATH, url[1], modelpath)
            newpath = path.relative( ROOT_PATH, newpath )
            return oldRender.bind(this)( newpath, extendedParams, ...params )
        }
    }
}

//routes
export import article = require('./article/pages')
app.use('/article', article.router )
app.use('/user', require('./user/pages'))
app.use('/admin', require('./admin/pages'))


app.get('/', (req, res) => {
    res.redirect(req.originalUrl + '/article/list')
})


//error handler
export function errorHandler(error, req, res, next) {
    if (error.code == 'UNAUTHORIZED')
        return res.redirect('/pages/user/login')

    console.error('Error on route ' + req.url, error)
    res.render('@error', { error })
}

app.use( errorHandler )