import mysql = require('mysql2')
import changeCase = require('change-case')

export var rawConnection
export var connection : MySql.Connection

const connectionParams = (poolSize, multipleStatements = false, nodatabase = false) => ({
    connectionLimit : poolSize ,
    host: process.env.APP_DB_HOST || '127.0.0.1',
    user: process.env.APP_DB_USER || 'root' ,
    database: nodatabase ? undefined : process.env.APP_DB_DB || 'blog-draft' ,
    password: process.env.APP_DB_PASS || '',
    trace: false ,
    multipleStatements
})


export async function init(multipleStatements = false, nodatabase = false) {
    if (rawConnection) return
    var pool = mysql.createPool(connectionParams(4, multipleStatements, nodatabase))
    rawConnection = pool

    var boundQuery = pool.query.bind(pool)
    var boundGetConn = pool.getConnection.bind(pool)
    connection = Object.assign({}, rawConnection, {
        async execute(...args) {
            return $promisify( boundQuery , ...args )
        },
        async getConnection(...args) {
            return $promisify( boundGetConn, ...args )
        }
    })
}


type insertOpts = {
    fields: string[]
    data: any
    into: string,
    camelCase?: boolean
}
export async function insert(p: insertOpts) {
    var outFields = p.camelCase ?
        p.fields.map(item => changeCase.snakeCase(item)) :
        p.fields

    outFields = outFields.map( f => mysql.escapeId(f) )

    var values = outFields.map((current, idx) => {
        let srcfield = p.fields[idx]
        var out = p.data[srcfield] !== undefined ? p.data[srcfield] : null
        if (typeof out === 'object' && !(out instanceof Date)) out = JSON.stringify(out)
        return out
    })

    let query = `INSERT INTO ${p.into}(${outFields.join(',')})
        VALUES (${Array(values.length).fill('?').join(',')})`

    return connection.execute(query, values)
}

type updateOpts = {
    fields: string[]
    data: any
    into: string,
    id: number
}
export async function simpleUpdate(p: updateOpts) {
    let setPairs = p.fields.map(field => {
        return `${field} = ?`
    })
    let values = p.fields.map(field => {
        let data = p.data[field]
        if (data === undefined) throw Error(`Update: field ${field} has no data.`)
        if (typeof data === 'object' && !(data instanceof Date)) data = JSON.stringify(data)
        return data
    })
    values.push(Number(p.id)||'')
    let query = `UPDATE ${p.into} SET ${setPairs.join(' , ')} WHERE id = ?`
    return connection.execute(query, values)
}