import { connection as db } from './database'

export async function tagList(articleId: number): Promise<{ id; name; selected; }[]> {
    articleId = Number(articleId)
    if (isNaN(articleId)) throw Error('Chamada inválida.')
    var [rows] = await db.execute(
        `SELECT tags.name AS name, tags.id AS id, a_t.article AS article
        FROM tags LEFT JOIN articles_tags a_t
		ON tags.id = a_t.tag AND a_t.article = ?`, [articleId])
    return rows.map(row => {
        return {
            id: row.id,
            name: row.name,
            selected: Boolean(row.article)
        }
     })
}


export async function updateTaglist(articleId: number, selected: any[]) {
    articleId = Number(articleId)
    if (!articleId) throw Error('Chamada inválida')
    var [conn] = await db.getConnection()
    var boundExec = conn.execute.bind(conn)

    try {
        await $promisify( conn.beginTransaction.bind(conn) )
        await $promisify(boundExec, 'DELETE FROM articles_tags WHERE article = ?',
            [articleId])
        var all = selected.map(async item => {
            await $promisify(
                boundExec, 'INSERT INTO articles_tags(article, tag) VALUES(?, ?)',
                [articleId, Number(item)])
        })
        await Promise.all(all)
        await $promisify( conn.commit.bind(conn) )
        conn.release()
    } catch (err) {
        await $promisify( conn.rollback(conn) )
        conn.release()
        throw err
    }
}


export async function createTag(name) {
    var [row] = await db.execute('INSERT INTO tags(name) VALUES (?)', [name])
    return { id : row.insertId }
}