import { connection as db } from './database'

export async function init() {
    var [rows] = await db.execute('SELECT id, title FROM articles WHERE slink IS NULL OR LENGTH(slink) = 0')
    if (rows.length) $log('Creating semantic link names for ' + rows.length + ' articles.')
    rows = rows.map(row => {
        row.slink = createSlink(row.title)
        return row
    })
    await Promise.all(rows.map(row => {
        return db.execute('UPDATE articles SET slink = ? WHERE id = ?', [row.slink, row.id])
    }))
}


export function createSlink(title: string) {
    return String(title).toLowerCase()
        .replace(/\s/g, '-')
        .replace(/[^a-z0-9-]/g, '')
        .substr(0, 49)
}


/**
 * Return a slink for an id if it is in the cache.
 * Requests caching of this info for next requests.
 */
var __slinkCache = new Map<number, string>()
var __slinkCacheCheckCount = 1
export function requestSlink(id) {
    var tryme = __slinkCache.get(id)
    if (tryme) return tryme
    db.execute('SELECT slink FROM articles WHERE id = ? LIMIT 1', [id]).then(([rows]) => {
        if (rows.length) {
            setSlinkCache(id, rows[0].slink)
        }
    })
}


export function setSlinkCache(id: number, slink: string) {
    __slinkCache.set(id, slink)
    __slinkCacheCheckCount = (__slinkCacheCheckCount + 1) % 100

    //reset the cache
    if (!__slinkCacheCheckCount && __slinkCache.size > 50) {
        var iter = __slinkCache.keys()
        for (var it = 0; it < __slinkCache.size - 30; it++) {
            let key = iter.next().value
            __slinkCache.delete(key)
        }
    }

}