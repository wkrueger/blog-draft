import moment = require('moment')
import { config } from '@common/config'
import ono = require('ono')
import { connection as db } from '@common/database'
import slink = require('@common/slink')
import marked = require('marked')
import highlight = require('highlight.js')
marked.setOptions({
    highlight: function (code) {
        return highlight.highlightAuto(code).value;
    }
})

type getPostOpt = {
    slink?
    id?
    session
    truncate?
    skip?
    count?
    selectTags?: string[]
}
export async function _showPosts(opts: getPostOpt) {
    var filters : string[] = []
    var params : any[] = []
    opts.selectTags = opts.selectTags || []

    let join = ''

    //status filter
    var i1 = !(opts.id || opts.slink)
    if ( i1 || !opts.session.userId ) {
        filters.push('a.status = "published"')
    }

    //by id
    opts.id = Number(opts.id)
    if (opts.id) {
        filters.push('a.id = ?')
        params.push(opts.id)
    }

    //by semantic link
    var useSlink = !opts.id && opts.slink
    if (useSlink) {
        filters.push('a.slink = ?')
        params.push(opts.slink)
    }

    //by tag
    if (opts.selectTags && opts.selectTags.length) {
        let list = opts.selectTags.map( t => Number(t) ).filter( t => t ).join(',')
        join = `JOIN articles_tags a_t ON a.id = a_t.article AND a_t.tag IN (${list})`
    }

    //contentField
    //opts.contentField = opts.contentField || 'a.content'
    let contentField = 'a.content'
    contentField = (opts.truncate) ? 'a.trimmed_content' : contentField
    let contentQuery = `${contentField} AS content,`

    params.push(Number(opts.skip) || 0, Number(opts.count) || 5)
    if (opts.slink || opts.id) opts.count = 1

    let query = `SELECT
    a.id, a.slink, a.title, a.created, ${contentQuery} custom_fields, a.markdown_break, u.display_name
        FROM articles a LEFT JOIN users u ON a.user = u.id ${join}
        ${filters.length ? 'WHERE' : ''} ${filters.join(' AND ')}
        ORDER BY created DESC
        LIMIT ?, ?`;

    //console.log(query)

    let [rows] = await db.execute(query, params)

    if (rows.length === 1) slink.setSlinkCache(Number(rows[0].id), rows[0].slink)

    //opts.raw is used for infinite scroll. In that case we don't want to append an error to the page.
    //if (!rows.length && !opts.raw) throw ono('Nada encontrado.')

    //if we provide both a slink and an id, we use the id. But if then they dont match, discard.
    if (opts.id && opts.slink && opts.slink !== (rows.length && rows[0].slink)) throw ono('Nada encontrado.')

    var all = rows.map(row => _processPost(row))
    var rows2 = await Promise.all(all)
    return { rows: rows2 }
}




async function _processPost(row) {
    var content = await $promisify(
        marked,
        (row.content||''),
        {
            sanitize: config.sanitize_markdown == true,
            breaks: row.markdown_break == true
        }
    )
    row.content = content
    row.formattedDate = moment(row.created).format(config.timestamp_format)
    row.userDisplayName = row.display_name
    return row
}