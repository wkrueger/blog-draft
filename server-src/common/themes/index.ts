import glob = require('glob')
import fs = require('fs')
import { connection as db, simpleUpdate } from '../database'
import _ = require('lodash')
import querystring = require('querystring')
import url = require('url')
import pages = require('./pages')
import tags = require('@common/tags')

export enum ModelType { list, slink }
global['ThemeModelType'] = ModelType


/**
 * Theme metadata memory store, populated on theme/route/etc refresh
 */
export var themes: Theme[] = []
export var themesIdx: { [key: string]: Theme } = {}
export var routes: Themes.Route[] = []
export var routesIdx: _.Dictionary<Themes.Route>
export var mainRoutesIdx: _.Dictionary<Themes.Route>
export var customFields: Themes.CustomField[] = []
export var customFieldsIdx: _.Dictionary<Themes.CustomField> = {}


/**
 *On server init.
 */
export async function init() {
    await refreshThemesFromDisk()
    await refreshRoutes()
    if (!routes.length) {
        await db.execute(`INSERT INTO routes(name, route, theme, model, main) VALUES("list","/", "vanilla", "main", 1)`);
        await db.execute(`INSERT INTO routes(name, route, theme, model, main) VALUES("slink","/link/:slink", "vanilla", "slink", 1)`);
        await refreshRoutes()
    }
}



export async function refreshThemesFromDisk() {
    var [allTags] = await db.execute('SELECT id, name FROM tags')
    var allTagsIdxName : any = _.keyBy(allTags, 'name')
    var [found] =
        await $promisify(glob, '../user-content/template-media/*/template.json')
    customFields = []
    var all = found.map( async pathname => {
        let split = pathname.split('/')
        let _reference = split[split.length-2]
        var json: Theme = await $promisify(fs.readFile, pathname)
        json = JSON.parse(String(json))
        customFields = [...customFields, ...(json.customFields || []).map(field => {
            field.theme = _reference
            return field
        })]


        json.scripts = (json.scripts || []).map(script => pages.themeresourceHelper(script, _reference))
        json.stylesheets = (json.stylesheets||[]).map(stylesheet => pages.themeresourceHelper(stylesheet, _reference))
        json.models = await Promise.all((json.models || []).map(async model => {
            model.type = <any>model.type === 'slink' ? ThemeModelType.slink : ThemeModelType.list
            model.scripts = (model.scripts || []).map(script => pages.themeresourceHelper(script, _reference))
            model.stylesheets =
                (model.stylesheets || []).map(stylesheet => pages.themeresourceHelper(stylesheet, _reference))
            model.feeds = await model.feeds.reduce(async (chain, feed) => {
                let out = await chain
                feed.selectTags = feed.selectTags || []
                feed.selectTags = await Promise.all(feed.selectTags.map(async tagname => {
                    let foundtag = allTagsIdxName[tagname]
                    if (found !== undefined) return foundtag.id
                    let { id } = await tags.createTag(tagname)
                    allTagsIdxName[tagname] = id
                    return id
                }))
                return [ ...out, feed]
            }, <any>Promise.resolve([]))
            model.provide = _.toPairs(model.provide || {}).map(([key, val]:[string,string[]]) => {
                var outvalue
                //linkhelper is dynamic, hence () => string
                if (val[0] == 'link') outvalue = ($customRoute) => {
                    return pages.linkHelper( ...(val.slice(1)),
                        { data: { root: { $customRoute } } }
                    )
                }
                return {
                    key,
                    value: outvalue || (() => '')
                }
            })
            return model
        }))
        return <any>Object.assign({
            _splashImg: `/template-media/${_reference}/splash.png` ,
            _reference
        }, json)
    })
    customFieldsIdx = _.keyBy(customFields, 'key')
    themes = await <any>Promise.all(all)
    themesIdx = _.keyBy(themes, '_reference')
}



async function refreshRoutes() {
    var [_routes] = await db.execute('SELECT * FROM routes ORDER BY seq')
    routes = _routes.map((routeObj: Themes.Route) => {
        if (routeObj.route.substr(-1) !== '/')
            routeObj.route = routeObj.route + '/'

        //find out if we have params
        var paramRgx = /:.*?(?=\/|\?)/g
        routeObj._foundParams = (routeObj.route.match(paramRgx) || [])
            .map(match => match.substr(1))

        //build the url matching regexp
        var testExpr = '^'
        var index = 0
        var iter
        while (iter = paramRgx.exec(routeObj.route)) {
            testExpr += routeObj.route.substring(index, iter.index)
            testExpr += '(.*?)'
            index = iter.index + iter[0].length
        }
        testExpr += routeObj.route.substr(index) + '$'
        routeObj._test = new RegExp(testExpr, 'i')

        //params are saved in querystring format.
        //convert to object for runtime
        var _param = querystring.parse(<any>routeObj.param) || {}
        routeObj.param = <any>{}
        for (var it in _param) {
            routeObj.param![it] = _param[it]
        }
        return routeObj
    })
    routesIdx = _.keyBy(routes, 'route')
    mainRoutesIdx = routes.filter(route => route.main).reduce((out, route) => {
        var key = route.theme + '||' + route.model
        out[key] = route
        return out
    }, {})
}



/**
 * Given a route url spec and params, merge them into the route url, also tell which ones were
 * actually taken or not.
 */
export function mergeParams<K>(
        route: Themes.Route,
        params: {[P in keyof K]: string }):
            { url: string; merged: {[P in keyof K]: boolean } } {

    var found = route._foundParams || []
    var outurl = route.route
    var merged: any = {}

    for (var key in params) {
        let exists = found.indexOf(key) !== -1
        if (exists) {
            outurl = outurl.replace(new RegExp(':' + key), params[key])
            merged[key] = true
        } else {
            merged[key] = false
        }
    }

    return {
        url: outurl,
        merged
    }
}



export async function setRoute(r: Themes.Route) {
    if (!r.route || !String(r.route).length) throw Error('Rota inválida.')
    var foundTheme = themesIdx[r.theme]
    if (!foundTheme) throw Error(`O tema ${r.theme} não foi encontrado.`)
    var foundModel = foundTheme.models.filter(model => model.key === r.model)
    if (!foundModel) throw Error(`O modelo ${r.model} não foi encontrado.`)
    await db.execute(`REPLACE INTO routes(main, name, route, theme, model, param, seq)
        VALUES(?, ?, ?, ?, ?, ?, seq)`, [r.main||0,r.name, r.route, r.theme, r.model, r.param||'', routes.length+1])
    await refreshRoutes()
}


export async function deleteRoute(id: number) {
    if (!id) throw Error('Id inválido.')
    await db.execute('DELETE FROM routes WHERE id = ?', [id])
    await refreshRoutes()
}


export async function upRoute(id: number) {
    let find = routes.filter(route => route.id === id)[0]
    let idx = routes.indexOf(find)
    routes.splice(idx, 1)
    routes.splice(idx - 1, 0, find)
    await sortRoutes()
}



export async function downRoute(id: number) {
    let find = routes.filter(route => route.id === id)[0]
    let idx = routes.indexOf(find)
    routes.splice(idx, 1)
    routes.splice(idx + 1, 0, find)
    await sortRoutes()
}


async function sortRoutes() {
    await Promise.all(routes.map( (route, idx) => {
        return simpleUpdate({
            data: { seq: idx + 1 },
            fields: ['seq'],
            into: 'routes',
            id: route.id!
        })
    }))
    await refreshRoutes()
}


export var customRoutesHandler = (req, res, next) => {
    for (var x = 0; x < routes.length; x++) {
        let parsed = url.parse(req.url)
        if (!parsed.pathname!.endsWith('/')) parsed.pathname += '/'
        req.url = url.format(parsed)
        let regxResult = routes[x]._test!.exec(parsed.pathname!)
        if (regxResult) {
            let currentRoute = routes[x]
            let currentTheme = themesIdx[currentRoute.theme]
            let currentModel =
                currentTheme.models.filter(model => model.key === currentRoute.model)[0]

            var params = {}
            if (currentRoute._foundParams!.length) {
                let items = regxResult.slice(1)
                params = currentRoute._foundParams!.reduce((out, param, idx) => {
                    out[param] = items[idx]
                    return out
                }, {})
            }

            req.customRoute = {
                route: currentRoute,
                theme: currentTheme,
                themeModel: currentModel,
                params: Object.assign({}, params, currentRoute.param)
            };
            if (!currentModel) return next()
            var type = currentModel.type
            if (type === ThemeModelType.slink) {
                return pages.slinkRoute(req, res, next)
            }
            else {
                return pages.listRoute(req, res, next)
            }
        }
    }
    next()
}