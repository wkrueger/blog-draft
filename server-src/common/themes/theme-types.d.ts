declare namespace Themes {

    export type RouteParams = {
        id?
        slink?
        skip?
        count?
        raw?
    }


    export interface Route {
        id?: number
        name: string
        route: string
        theme: string
        model: string
        param?: RouteParams
        main: number
        _foundParams?: string[]
        _test?: RegExp
    }


    export interface Theme {
        _reference;
        _splashImg;
        name;
        description;
        models: ThemeModel[]
        scripts?: string[]
        stylesheets?: string[]
        customFields?: CustomField[]
    }


    export interface ThemeModel {
        key: string
        name: string
        template: string
        type: ThemeModelType;
        raw?: boolean
        scripts: string[]
        stylesheets: string[]
        /** in the JSON, provide is string[]. Then mapped to the type below on load */
        provide: {
            key: string;
            value: (root) => string;
        }[]
        feeds: {
            key: string;
            skip?: number;
            count?: number;
            truncate?: boolean;
            main?: boolean
            selectTags?: any[]
            selectFields?: string[] //currently not used
        }[]
    }


    /**
     * This is added to the express.Request
     */
    export interface TemplateCustomRoute {
        route: Route,
        theme: Theme,
        themeModel: ThemeModel
        params: RouteParams
    }


    /**
     * Variables available in every hbs template.
     */
    export interface TemplateRoot {
        /** is logged in */
        $online: boolean
        $userName: string
        /** has the livereload env var set */
        $liveReload: boolean
        $blogTitle: string
        $blogSubtitle: string
        $currentUrl: string
        $baseUrl: string
        /** current route and template info */
        $customRoute: TemplateCustomRoute
        $provide: { key: string; value: string; }[]
        $stylesheets: string[]
        $scripts: string[]
    }


    export interface CustomField {
        theme: string;
        key: string;
        label: string;
        type: 'text' | 'image' | 'textarea'
        value? : string
    }

}

declare enum ThemeModelType {
    list, slink
}

declare type Theme = Themes.Theme