import { _showPosts } from './processor'
import express = require('express')
import { config } from '@common/config'
import qs = require('qs')
import themes = require('./index')
import slink = require('@common/slink')
import _ = require('lodash')


export function themeresource(path, themeref) {
    return '/template-media/' + themeref + '/' + path
}

export function themeresourceHelper(...params) {
    var path = params[0]
    var context = params.pop()
    var themeref = (typeof context === 'object') ?
        context.data.root.$customRoute.theme._reference :
        context
    return themeresource(path, themeref)
}

/**
 * DEBUG
 */
export function inflateHelper(...params) {
    var path = params[0]
    var context = params.pop()
    var themeref = (typeof context === 'object') ?
        context.data.root.$customRoute.theme._reference :
        context
    let toload = themeresource(path, themeref)
    let file = require('fs').readFileSync(process.cwd() + '/../user-content' + toload).toString()
    return file
}


export var listRoute : express.RequestHandler = async (req, res, next) => {
    try {
        let rt = req.customRoute;
        if (!rt) throw Error('Um tema precisa de uma View.')
        let view = `#${rt.theme._reference}/views/${rt.themeModel.template}`
        let customParam = req.customRoute.params || {}
        var torender = commonParams(req, true)

        var all = req.customRoute.themeModel.feeds.map(async feed => {
            if (!feed.key) throw Error('Um grupo precisa de um identificador ("chave").')
            var { rows } = await _showPosts({
                skip: feed.skip || customParam.skip || req.query.skip,
                count: feed.count || customParam.count || req.query.count,
                truncate: (feed.truncate !== undefined) ? feed.truncate : true,
                session: req.session,
                selectTags: feed.selectTags
            })
            torender[feed.key] = rows
        })

        await Promise.all(all);
        res.render(view, torender)
    } catch (err) {
        next(err)
    }
}


export async function slinkRoute(req:express.Request, res, next) {
    try {
        let rt = req.customRoute;
        if (!rt) throw Error('Um tema precisa de uma View.')
        let view = `#${rt.theme._reference}/views/${rt.themeModel.template}`
        let customParam = req.customRoute.params || {}
        var torender = commonParams(req, true)

        var all = req.customRoute.themeModel.feeds.map(async feed => {
            if (!feed.key) throw Error('Um grupo precisa de um identificador ("chave").')
            var rows
            if (feed.main) {
                ({ rows } = await _showPosts({
                    count: 1,
                    id: customParam.id || req.query.id,
                    slink: customParam.slink || req.params.slink,
                    session : req.session
                }))
            } else {
                ({ rows } = await _showPosts({
                    skip: feed.skip || customParam.skip || req.query.skip,
                    count: feed.count || customParam.count || req.query.count,
                    truncate: (feed.truncate !== undefined) ? feed.truncate : true,
                    session: req.session ,
                    selectTags : feed.selectTags
                }))
            }
            torender[feed.key] = rows
        })

        await Promise.all(all)
        res.render( view, torender )
    } catch (err) {
        next(err)
    }
}


/**
 * {{link model <model> theme <theme> slink <slink>}}
 */
export function linkHelper(...params) {
    var context = params.pop()
    var root: Themes.TemplateRoot = context.data.root
    var hash = _.chain(params).chunk(2).fromPairs().value()

    var route: Themes.Route | undefined
    if (hash.route) route = themes.routesIdx[hash.route]
    if (!route) {
        let model = hash.model || _.get(root, ['$customRoute','route','model']) || 'main'
        let theme = hash.theme || _.get(root, ['$customRoute','route','theme']) || 'vanilla'
        route = themes.mainRoutesIdx[theme + '||' + model]
        if (!route) return 'UNABLE-TO-RESOLVE-LINK'
    }

    //tries to also add the slink, if its in the cache
    if (hash.id && !hash.slink) hash.slink = slink.requestSlink(hash.id)

    var { url, merged:mergestatus } = themes.mergeParams(route, hash)
    var nonmerged = {}
    var exclude = ['model','route','theme']
    for (var it in mergestatus) {
        if (exclude.indexOf(it) !== -1) continue;
        if (!mergestatus[it] && hash[it]) nonmerged[it] = hash[it]
    }
    var query = qs.stringify(nonmerged)
    return url + '?' + query
}


function commonParams(req:express.Request, truncate:boolean) {
    var customRouteParams = req.customRoute.params
    let isRaw = customRouteParams.raw || req.customRoute.themeModel.raw || req.params.raw || req.query.raw
    var skipped = customRouteParams.skip || req.query.skip || 0
    var noRoll = (customRouteParams.slink || req.params.slink || customRouteParams.id || req.query.id)
        ? true : false
    var out = {
        title : config.main_title ,
        //items: rows2,
        $atRoot: true ,
        skipped  ,
        noRoll ,
        truncated: truncate,
        taglist: [],
        $stylesheets: [
            ...req.customRoute.theme.stylesheets!,
            ...req.customRoute.themeModel.stylesheets
        ],
        $scripts: [
            ...req.customRoute.theme.scripts!,
            ...req.customRoute.themeModel.scripts
        ],
        $provide: req.customRoute.themeModel.provide
            .map(item => {
                return { key: item.key, value: item.value(req.customRoute) }
            })
    }
    if (isRaw) out['layout'] = 'flat-layout'
    return out
}