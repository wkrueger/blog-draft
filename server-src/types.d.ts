declare var $debugMw : any
declare var $checkParams : (obj:any, ...params:string[]) => any
declare var $log : any
declare var $promisify : any
declare var escape : (s:string) => string

declare module NodeJS {
    interface Global {
    }
}


declare namespace Express {
    //add custom items to session
    export interface Session {
        userId: number
        userName: string
    }

    //declare undocumented member
    export interface Response {
        req : Express.Request
    }

    export interface Request {
        customRoute: Themes.TemplateCustomRoute
        originalUrl?: string
    }
}


declare namespace MySql {
    export interface Connection {
        execute(query: string, params?: any): Promise<any>;
        getConnection(): Promise<any>
        escape(i:any) : string
    }
}