/* eslint-env browser */
/* global $, Page, Site */

$(function () {
    var __skipped = Page.skipped
    var $infinite = $('.infinite-s')
    var $infiniteLoader = $infinite.find('> .segment')


    if (Page.noRoll) {
        $infinite.remove()
    } else {
        $infinite.visibility({
                once: false ,
                onTopVisible : function() {
                    $infiniteLoader.dimmer('show')
                    let nextSkip = Number(__skipped) + Number($('.post.segment').length)
                    Site.ajaxHtml(
                        { url : Page.provide.infiniteScrollLink + '&skip=' + nextSkip}
                    ).then(function(html) {
                        $infiniteLoader.dimmer('hide')
                        var $next = $(html)
                        if (!$next.length) {
                            $infinite.remove()
                            return
                        }
                        enhancePost($next)
                        $infinite.before($next)
                    })
                    .catch(function(error) {
                        $infiniteLoader.dimmer('hide')
                        console.error(error)
                        $infinite.remove()
                        //throw error
                    })
                }
            })
    }

    var $fixedMenu = $('.fixed.menu')
    $('.mhead').visibility({
        once : false ,
        onBottomPassed: function() {
            $fixedMenu.transition('fade in')
        } ,
        onBottomPassedReverse: function() {
            $fixedMenu.transition('fade out')
        }
    })

    function enhancePost($inp) {
        var $content = $inp.find('.article-content')
        $content.find('pre').addClass('ui segment')
        $content.find('table').addClass('ui collapsing table')
    }

    enhancePost($(document.body))
})